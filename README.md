[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fleischmann-lab%2Fdatasets%2Fdaste-odor-set-2021-11/HEAD?labpath=demo.ipynb)

# Showcase of the _Daste odor set 2021-11_ dataset

## Background

This repository showcases how to extract the data from the [Daste odor set 2021-11](https://dandiarchive.org/dandiset/000167/0.220329.0720).

Please cite as:
> Daste, Simon; Pierré, Andrea (2022) Two photon calcium imaging of mice piriform cortex under passive odor presentation (Version 0.220928.1306) [Data set]. DANDI archive. https://doi.org/10.48324/dandi.000167/0.220928.1306

## Requirements

This project needs to have Python and Pip already installed, which you can get from [Anaconda](https://www.anaconda.com/products/distribution).

## Setup

You can manually download this project or clone it with Git:

``` bash
git clone https://gitlab.com/fleischmann-lab/datasets/daste-odor-set-2021-11.git
```

After cloning the project, create a virtual environment and install the required packages with the following commands:

``` bash
cd daste-odor-set-2021-11        # after cloning like above
python -m venv .venv             # create virtual environment via venv
source .venv/bin/activate        # activate environment
pip install -r requirements.txt  # install dependencies
```

## Running the _demo_ notebook

After installation is completed, you should be able to load the _demo_ notebook inside the Jupyter interface. To start Jupyter, type:

``` bash
jupyter lab
```

## Credits

Pipeline used to convert the dataset to NWB: <https://gitlab.com/fleischmann-lab/calcium-imaging/calimag>
