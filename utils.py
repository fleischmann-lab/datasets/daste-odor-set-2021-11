import time
import functools
import numpy as np
import pandas as pd
from scipy import signal
from tqdm import tqdm
from sklearn.preprocessing import StandardScaler

import matplotlib.pyplot as plt
import seaborn as sns

def butter_filter(data, Fs, order, low=None, high=None):
    """
    Filter `data` (sampled at `Fs` Hz) with Butterworth filter `order`
    - `low`: low cut [Hz], allowing only higher, i.e. highpass if not `None`
    - `high`: high cut [Hz], allowing only lower, i.e. lowpass if not `None`
    Assume bandpass [`low`, `high`] if both are not `None`
    """
    Fnq = Fs / 2
    has_low, has_high = low is not None, high is not None
    assert has_low or has_high, 'Need at least `low` or `high`' 
    if has_low and not has_high:
        args = [low/Fnq, 'highpass']
    elif has_high and not has_low:
        args = [high/Fnq, 'lowpass']
    else: 
        assert low < high
        args = [[low/Fnq, high/Fnq], 'bandpass']
    b, a = signal.butter(order, *args)
    data = signal.filtfilt(b, a, data)
    return data


def construct_gausswin(Fs, size_sec=1, std_factor=0.1):
    """
    Construct Gaussian window for signal sampled at `Fs` Hz
    of size `size_sec` [default: `1`] seconds (to get # points) 
    and stdev scaled by `std_factor` [default: `0.1`] from # points
    """
    num_points = round(Fs * size_sec)
    win_std = std_factor * num_points
    win = signal.windows.gaussian(num_points, win_std)
    win /= win.sum()
    return win


def split_by_trial(nwb, data, time, axis=0, reshift_time=True):
    """
    Split `data` (in `axis` [default: `0`] axis as `data` time axis) 
    extracted from `nwb` object into trials, using `time` array.
    
    If `reshift_time` [default: `True`], each trial's time vector
    will be offset to begin at 0.
    
    Returns: list of `data_by_trial` and `time_by_trial`
    """
    data_by_trial, time_by_trial = [], []
    for _, row in nwb.trials.to_dataframe().iterrows():
        trial = (time >= row.start_time) & (time <= row.stop_time)
        _data = np.take(data, np.where(trial)[0], axis=axis)
        _time = time[trial]        
        if reshift_time:
            _time -= _time[0]            
        data_by_trial.append(_data)
        time_by_trial.append(_time)
    return data_by_trial, time_by_trial 


def timer(desc=None, strcat=True):
    """
    Helper decorator to report exec timings of other functions
    - `desc`: what to print when starting [default: `None`]
    - `strcat`: when finish, whether to print on same line [default: `True`]
    """
    def decorator_timer(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            mesg = desc if desc else f'{func.__name__!r}'
            if strcat:
                start_print_kwargs = {'end': ''}
                end_print_desc = ''
            else:
                start_print_kwargs = dict()
                end_print_desc = f'\t[{mesg}] ...'
                
            t0 = time.time()
            print(desc + ' started ...', **start_print_kwargs)
            
            value = func(*args, **kwargs)
            
            t1 = time.time()
            elapsed = t1-t0
            print(f'{end_print_desc} done ({elapsed:.2f} seconds)')
            
            return value
        return inner
    return decorator_timer
 
    
@timer(desc='Odor identity extraction')
def extract_odor(nwb):
    """
    Extract odor identities from `nwb` object per trial
    Returns: a `numpy.array` of odors, each element is a trial
    """
    odor = nwb.stimulus['odor']
    odor, _ = split_by_trial(nwb, odor.data[:], odor.timestamps[:])
    odor = list(map(np.unique, odor))
    assert all([len(x) == 1 for x in odor]), 'Some trials have multiple odors'
    return np.array(odor).flatten().astype('int')


@timer(desc='Sniff rate computation')
def compute_sniff(nwb, filter_kwargs=dict(), peak_kwargs=dict(), gauss_kwargs=dict()):
    """
    Compute sniff rate from `nwb` object per trial
    - `filter_kwargs`: filter option dict 
        - default: `dict(order=2, low=2, high=30)`
        - see `butter_filter` function for description of `oder`, `low`, `high`
    - `peak_kwargs`: peak detection option dict (after z-score)
        - default: `dict(use_negative=True, prominence=1, distance_sec=0.04)`
        - `use_negative`: invert signal to find peaks
        - `prominence`: peak prominence after z-score normalization 
        - `distance_sec`: peak distance in seconds
    - `gauss_kwargs`: gaussian window construction option dict for smoothing
        - default: `dict(size_sec=2, std_factor=0.1)`
        - see `construct_gausswin` for description on `size_sec` and `std_factor`
    Returns: a dict of list of trials of `rate` (sniff rate in Hz) and `t` time vec    
    """
    # get raw sniff data
    sniff = nwb.acquisition['flow']
    sniff, t = sniff.data[:], sniff.timestamps[:]
    dt = t[1] - t[0]
    Fs = 1 / dt
    
    # default kwargs 
    def_filter_kwargs = dict(order=2, low=2, high=30)
    def_peak_kwargs = dict(use_negative=True, prominence=1, distance_sec=0.04)
    def_gauss_kwargs = dict(size_sec=2, std_factor=0.1)
    
    # update kwargs
    filter_kwargs = dict(**def_filter_kwargs, **filter_kwargs)
    peak_kwargs   = dict(**def_peak_kwargs, **peak_kwargs)
    gauss_kwargs  = dict(**def_gauss_kwargs, **gauss_kwargs)
    
    # some processing of kwargs 
    peak_kwargs['distance'] = Fs * peak_kwargs['distance_sec']
    use_negative_peaks = peak_kwargs.get('use_negative', True)
    del peak_kwargs['distance_sec'], peak_kwargs['use_negative']
    assert gauss_kwargs['size_sec'] * Fs < len(t), 'gauss win size too big'

    # z-scale -> filter -> peak
    sniff = StandardScaler().fit_transform(sniff[:,None]).flatten() 
    sniff = butter_filter(sniff, Fs=Fs, **filter_kwargs)
    if use_negative_peaks: sniff = -sniff
    pk_loc, _ = signal.find_peaks(sniff, **peak_kwargs)
    
    # events -> rate
    events = np.zeros(len(t))
    events[pk_loc] = 1 / dt # so each peak has unit in second
    win = construct_gausswin(Fs, **gauss_kwargs)
    rate = np.convolve(events, win, mode='same')
    
    # split trial
    rate, t = split_by_trial(nwb, rate, t)
    
    return dict(rate = rate, t = t)


def norm_F_vec(v, Fs, tau_m=1, tau_q=20, q=0.1, div=True, bidir=False):
    """
    Normalize fluorescience vector `v` sampled at `Fs` Hz with median filter 
    of size `tau_m` [default: `1`] second. 
    
    Then use moving window of size `tau_q`[default: `20`] second to calculate 
    baseline `v0` from quantile `q` [default: `0.1`] of `v`. If `tau_q < 0`, 
    will use the whole trace, recommended for short traces of < 30 seconds. 
    
    Then normalize by doing `v - v0`.
    - If `div` [default: `True`], will be `(v - v0) / v0`
    - If `bidir` [default: `False`, not recommened `True`], will do the 
        median filter and quantile normalization in the opposite direction, 
        no division or further birectional normalization
        
    Note: this can definitely be improved with vectorization efficiently
    """
    km, kq = round(Fs * tau_m), round(Fs * tau_q)
    L = len(v)
    v = signal.medfilt(v,  2*round(km/2) + 1)
    if kq < 0:
        v0 = np.quantile(v,q)
    else:
        v0 = [np.quantile(v[:kq], q)] * kq    
        for i in range(kq, L):
            v0.append(np.quantile(v[i-kq:i], q))
        v0 = np.array(v0)
    v -= v0
    if div: 
        v /= v0
    if bidir:
        v = norm_F_vec(v[::-1], Fs, tau_m, tau_q, q, div=False, bidir=False)[::-1]
    return v


def norm_F_mat(V, *args, **kwargs):
    """
    Normalize a matrix `V` of `num_rois x num_times` size
    with the same arguments as in `norm_F_vec`
    Note: this can definitely be improved with vectorization efficiently
    """
    V = np.array([
        norm_F_vec(v, *args, **kwargs)
        for v in V
    ])
    return V


@timer(desc='dF/F0 computation', strcat=False)
def compute_dFFO(nwb, 
                 k_neu=0.7, 
                 combine_planes=True, 
                 use_iscell=True, 
                 split_trial=True, 
                 norm_F=True, 
                 norm_kwargs=dict()
                ): 
    """
    Compute dF/F0 from fluorescence data inside `nwb`, separated by trials if `split_trial`
    - `k_neu`: [default: `0.7`] scalar to subtract neutropil, i.e. `F = F - k_neu x F_neu`
    - `combine_planes`: [default: `True`] whether to combine all the planes in one dataset.
        Warning: the timings of different planes are not aligned, and 
        and there is no alignment/reshifting/resampling happening here.
        If `False`, there will be a field in the sub-dict to indicate each field
    - `use_iscell`: [default: `True`] whether to use `iscell` from `suite2p` output
    - `split_trial`: [default: `True`] whether to split by trial, recommended 
        as normalization done on a trial basis is better 
        (for baseline estimation during normalization).
        Note: currently to split trials, the timestamps of the first plane is used,
        see <https://gitlab.com/fleischmann-lab/calcium-imaging/calimag/-/issues/92>
    - `norm_F`: [default: `True`] whether to normalize using `norm_F_vec` function with 
        arguments from a dict of `norm_kwargs` (see `norm_F_vec` for more info).
        If `False`, will do only `(F - F0) / F0` with `F0 = k_neu x F_neu`.
        This can only be `True` if `split_trial` is also `True`.
            
    Returns: a dict of `info` (the params), `t` (time) and `dFF0` data of
    - (not `split_trial`, not `combine_planes`): each sub-dict of planes contains `rois_per_plane x all_times`
    - (`split_trial`, not `combine_planes`): each sub-dict of planes contains `list[rois_per_plane x times_per_trial]` (`len = num_trials`)
    - (not `split_trial`, `combine_planes`): array of `all_rois x all_times`, by concatenating ROIs across planes
    - (`split_trial`, `combine_planes`): `list[all_rois x times_per_trial]` (`len = num_trials`)    
    """
    
    F = nwb.processing['ophys']['Fluorescence']
    Fneu = nwb.processing['ophys']['Neuropil']
    planes = sorted(F.roi_response_series.keys())
    
    if use_iscell: 
        iscell = nwb.processing['ophys']['ImageSegmentation']['PlaneSegmentation']['iscell'].data[:,0] > 0
        nc_roi = 0
    
    output = dict(
        dFF0 = dict(),
        t = dict(),
        info = dict(
            k_neu = k_neu,
            combine_planes = combine_planes,
            use_iscell = use_iscell,
            split_trial = split_trial,
            norm_F = norm_F,
            norm_kwargs = norm_kwargs
        )
    )
    
    for i, p in enumerate(planes):
        F_p = F[p].data[:]
        Fn_p = Fneu[p].data[:]
        t_p = F[p].timestamps[:]
        assert all(t_p - Fneu[p].timestamps[:] == 0)
        Fs = 1/(t_p[1] - t_p[0])
        
        if i == 0: 
            p0, t_p0 = p, t_p
            
        if use_iscell:
            iscell_mask = iscell[nc_roi:nc_roi+F_p.shape[0]]
            nc_roi += F_p.shape[0]
            
            F_p = F_p[iscell_mask]
            Fn_p = Fn_p[iscell_mask]
            
        F0_p = k_neu * Fn_p
        activ_p = F_p - F0_p
        
        if not norm_F:
            activ_p /= F0_p
            
        if split_trial:
            # reference time is from first plane, 
            # currently plane_3 last time per trial is tinily > stop_time of that trial
            activ_p, t_p = split_by_trial(nwb, activ_p, t_p0, axis=1)
        
        if norm_F: 
            assert split_trial, 'can only norm when already split by trials'
            activ_p = [norm_F_mat(X, Fs, **norm_kwargs) for X in tqdm(activ_p, desc=p)]

        output['dFF0'][p] = activ_p       
        output['t'][p] = t_p
    
    if combine_planes:
        # TODO: raise warning
        if not split_trial:
            output['dFF0'] = np.concatenate(list(output['dFF0'].values()))
        else:
            # this might be better handled by xarray 
            num_trials = len(output['dFF0'][p0])
            output['dFF0'] = [
                np.concatenate(list(
                    map(lambda x: x[trial], 
                        output['dFF0'].values()
                       )))
                for trial in range(num_trials)
            ]
        output['t'] = output['t'][p0]

    return output


@timer(desc='Aggregate data by trial')
def aggregate_data_by_trial(odor, sniff, dFF0, align_method = 'interp'):
    """
    Aggregate `odor` identity (from `extract_odor`), `sniff` rate (from `compute_sniff`)
    and `dFF0` (from `compute_dFFO`, assumed `combine_planes = split_trial = True`).
    
    Because `sniff` and `dFF0` are samppled at different rate, the former will be
    either interpolated `align_method = "interp"` [default] or resampled `align_method = "resample"`. 
    Note: Because the end times do not align, using "resample" might induce some phase shift.
    
    Returns: a list of dict of trial data of
    - `t` (time based on `dFF0`)
    - `odor` scalar of odor id
    - `sniff` vector after alignment
    - `dFF0` matrix (same as `dFF0`)
    """
    df_info = dFF0['info']
    assert df_info['combine_planes'] and df_info['split_trial'], \
        "`dFF0` data need to have `combine_planes = split_trial = True`"
        
    align_method = align_method.lower()
    assert align_method in ['interp', 'resample'], \
        "Only 'interp' or 'resample' allowed as `align_method`"
        
    num_trials = len(odor)
    assert num_trials == len(sniff['t']) \
            and num_trials == len(dFF0['t']), \
        "Number of trials mismatch between these 3 data sources"
    
    output = []
    for trial, _odor_id in enumerate(odor):
        trial_output = dict()
        
        _sniff_rate = sniff['rate'][trial]
        t_sniff = sniff['t'][trial]
        t_F = dFF0['t'][trial]
        
        if align_method == 'interp':
            _sniff_rate = np.interp(t_F, t_sniff, _sniff_rate)
        else:
            _sniff_rate = signal.resample(_sniff_rate, len(t_F), t_sniff)
        
        trial_output = dict(
            t = t_F,
            odor = _odor_id,
            sniff = _sniff_rate,
            dFF0 = dFF0['dFF0'][trial]
        )
        
        output.append(trial_output)
    return output


def time_aggregate(X, t, win_sec=5, axis=0, fn=np.mean):
    """
    Break an array/matrix `X` into chunks (using time `t` and `win_sec` window)
    and aggregate data for each chunk using `fn` function [default: `np.mean`] 
    with `X`'s `axis` indicate its time axis [default: `0`, currently only accept `0` or `1`]
    
    `win_sec`: [default: `5`] window in seconds to determine how to split `t` vector into chunks
    - If scalar, chunks of `win_sec` non-overlapping windows,
        Returns: arrays of data (aggregated with `fn`) and `t` (aggregated by mean)
    - If dict of list, e.g `dict(base=[0,10], stim=[10,15], post=[15,np.inf])`, 
        will use the list to separate times (inclusive lower, exclusive upper, e.g. `t >= 0 & t < 10`),
        Returns: dicts of data and time based on the structure of the given dict.
        
    Note: this can use more improvement when dealing with high-dim array
    """
    type_win = type(win_sec)
    assert axis in [0, 1], 'only accept `axis` = 0 or 1'
    assert callable(fn), '`fn` has to be a callable function'
    
    if type_win in [float, int]:
        win_k = round(win_sec / (t[1] - t[0]))
        split_indices = np.arange(win_k, len(t), win_k)
        split_indices = split_indices[split_indices < len(t)]

        t = np.array_split(t, split_indices)
        X = np.array_split(X, split_indices, axis=axis)

        t_out = np.array(list(map(np.mean, t)))
        X_out = np.array([fn(x, axis=axis) for x in X])
        if axis == 1: X_out = X_out.T # not very generalizable
        
    elif type_win is dict:
        assert all([(len(x) == 2 and all(np.diff(x) > 0)) 
                    for x in win_sec.values()]), \
            "All values inside `win_sec` dict needs to be an increasing 2-element dict"
        X_out, t_out = dict(), dict()
        min_t, max_t = t.min(), t.max()
        
        for label, win in win_sec.items():
            win = np.array(win) # for saving purposes
            win[np.isneginf(win)] = min_t
            win[np.isposinf(win)] = max_t
            
            # filter
            loc_t = np.squeeze(np.where((t >= win[0]) & (t < win[1])))
            X_win = np.take(X, loc_t, axis=axis)
            
            # save and aggregate
            t_out[label] = win # keep as record, no averaging
            X_out[label] = fn(X_win, axis=axis)
            
    else:
        raise ValueError('`win_sec` can only be a scalar or a dict')
        
    return X_out, t_out


def time_aggregate_by_trial(data, win_sec=5, stiff_fn=np.mean, dFF0_fn=np.max):
    """
    Aggregate activity with time of `data` from `aggregate_data_by_trial` using `time_aggregate`
    - `win_sec`: see `time_aggregate` function for details [default: `5` seconds]
    - `stiff_fn`: function to time aggregate sniff rate [default: `np.mean`]
    - `dFF0_fn`: function to time aggregate dFF0 [default: `np.max`] 
    """
    output = []
    for dat in data:
        t = dat['t']
        sniff, _ = time_aggregate(dat['sniff'], t, win_sec=win_sec, fn=stiff_fn)
        dFF0, t = time_aggregate(dat['dFF0'], t, win_sec=win_sec, axis=1, fn=dFF0_fn)
        output.append(dict(
            t = t,
            odor = dat['odor'],
            sniff = sniff, 
            dFF0 = dFF0
        ))
    return output

def plot_activity_of_roi(roi, data, figsize=(25,10), z_sem=1.96):
    """
    Plot dFF0 from `data` (from `aggregate_data_by_trial` output) of 
    a select ROI index `roi`, with each subplot an odor, with all traces,
    as well as the mean and the shading with `z_sem` [default: `1.96`] width.
    """
    plt.figure(figsize=figsize)
    odor = np.array([x['odor'] for x in data])
    
    for i, odor_id in enumerate(np.unique(odor)):
        if i == 0:
            ax = plt.subplot(2,5,i+1)
        else:
            ax = plt.subplot(2,5,i+1, sharex=ax, sharey=ax)
        odor_trials = np.where(odor == odor_id)[0]
        
        trial_dFF0 = []

        for trial in odor_trials:
            trial_data = data[trial]
            assert odor_id == trial_data['odor']
            trial_t = trial_data['t']

            dFF0_of_trial = trial_data['dFF0'][roi]
            trial_dFF0.append(dFF0_of_trial)

        trial_dFF0 = np.array(trial_dFF0).T
        activ_mean = trial_dFF0.mean(axis=1)
        activ_err = z_sem * trial_dFF0.std(axis=1) / np.sqrt(trial_dFF0.shape[1])
        lower = activ_mean - activ_err
        upper = activ_mean + activ_err
        
        ax.plot(trial_t, trial_dFF0, '-k', lw=1, alpha=0.1)
        ax.plot(trial_t, activ_mean, '-b', lw=3)
        ax.fill_between(trial_t, lower, upper,
                        interpolate=True, color='b', 
                        alpha=0.2, edgecolor='none')
        
        ax.set_xlabel('time (sec)')
        ax.set_ylabel('activ (d F/F0)')
        ax.set_title(f"odor = {odor_id} {f'(ROI {roi})' if i == 0 else ''}")

    sns.despine(trim=True)
    plt.tight_layout()
    plt.show()